# youtube-dl termux
list of commands to install [youtube-dl](https://github.com/ytdl-org/youtube-dl) and [set presets](https://gitlab.com/mriutrt/youtube-dl-presets) with [termux](https://termux.com/)


## commands
upgrade termux's packages
```bash
pkg upgrade
```

install ffmpeg, for transcoding
```bash
pkg install ffmpeg
```

install python, for youtube-dl
```bash
pkg install python
```

download youtube-dl in `$PREFIX/bin` (`$PREFIX` is a constant set by termux, it leads to `/data/data/com.termux/files/usr`)
```bash
curl -L https://yt-dl.org/downloads/latest/youtube-dl -o $PREFIX/bin/youtube-dl
```

give read and execute rights to all
```bash
chmod a+rx $PREFIX/bin/youtube-dl
```

write presets in `$PREFIX/bin/yt`. MP3s will be stored in `downloads/MP3` and videos will be stored in `downloads/Videos` (you can modify this at will, just make sure the path exists)
```bash
echo '#!/bin/bash
######################################
#                                    #
#    dl media content, vid or mp3    #
#                                    #
#          uses youtube-dl           #
#                                    #
######################################

# this script helps you use presets in order to easily download your media
# /!\ it cannot function without youtube-dl
# https://github.com/ytdl-org/youtube-dl

function usage(){
 echo "Usage : $0 [option] [URL]" >&2
 echo "Options:"
 echo "        [-h]              Prints youtube-dls help"
 echo "        [-m]              Downloads mp3 according to presets set inside this script"
 echo "        [-v]              Downloads video according to presets set inside this script"
 echo "        [-u]              Updates youtube-dl"
 echo "        [-y]              Launches youtube-dl with your own options"
 echo "                          - do not forget to specify -o destination/directory"
}

while getopts "m:M:v:V:y:Y:uUhH" option
do
 case $option in
   m|M)
    $PREFIX/bin/youtube-dl $OPTARG -x --audio-format mp3 --audio-quality 320K -o "storage/downloads/MP3/%(artist)s_-_%(title)s.%(ext)s"
    ;;
   v|V)
    $PREFIX/bin/youtube-dl $OPTARG --write-sub --sub-format srt --sub-lang en --convert-subs srt --embed-subs --recode-video mp4 -o "storage/downloads/Videos/%(title)s.%(ext)s"
    ;;
   u|U)
    $PREFIX/bin/youtube-dl --update
    ;;
   h|H)
    $PREFIX/bin/youtube-dl --help
    ;;
   y|Y)
    $PREFIX/bin/youtube-dl $OPTARG
    ;;
   \?)
    usage
    ;;
 esac
done

if [ $OPTIND -eq 1 ]
  then usage
  exit 1
fi
exit 0
' > $PREFIX/bin/yt
```

give read and execute rights to all
```bash
chmod a+rx $PREFIX/bin/yt
```


tell `.bashrc` that you might have a file named `.bash_aliases` stored in `$HOME` (`$HOME` is a constant set by termux, it leads to `/data/data/com.termux/files/home`) and sources it if it exists
```bash
echo "
if [ -f $HOME/.bash_aliases ]; then
    . $HOME/.bash_aliases
fi
" >>$PREFIX/etc/bash.bashrc
```


add an alias `yt` inside the `.bash_aliases` file that leads to the real `yt` in `$PREFIX/bin`, so that you can use it from wherever, like a normal termux command
```bash
echo '
alias yt="bash $PREFIX/bin/yt"
' >>$HOME/.bash_aliases
```
